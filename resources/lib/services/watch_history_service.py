from time import time

from resources.lib.api.api import API
from resources.lib.const import SERVICE, WATCH_HISTORY, ROUTE
from resources.lib.kodilogging import service_logger
from resources.lib.routing.router import router
from resources.lib.services import TimerService
from resources.lib.storage.sqlite import DB
from resources.lib.utils.kodiutils import current_path, refresh
from resources.lib.utils.url import Url


class WatchHistoryService(TimerService):
    SERVICE_NAME = SERVICE.LIBRARY_SERVICE

    def __init__(self, api, *args, **kwargs):
        super(WatchHistoryService, self).__init__(*args, **kwargs)
        self.api = api
        self.items_to_watch = []
        self.last_sync = None
        self.is_paused = False

    def sync_db(self):
        if not DB.FILES.active or self.is_paused or (self.last_sync and time() - self.last_sync < 10):
            return self.items_to_watch
        self.last_sync = time()
        service_logger.info("Syncing watch history...")
        filtered = []
        files = DB.FILES.get_unfinished()
        if len(files) == 0:
            self.items_to_watch = filtered
            return filtered
        watched = DB.WATCH_HISTORY_ADVANCED.get_all()
        files_map = {}
        watched_map = {}

        watched_to_delete = []
        watched_to_add = []

        for item in files:
            files_map[item[2]] = item

        for item in watched:
            watched_map[item[1]] = item
            if item[1] not in files_map:
                watched_to_delete.append(item[0])

        for item in files:
            if item[2] not in watched_map:
                watched_to_add.append(item[2])

        ids = [dict(Url.get_qs(i))['media_id'] for i in watched_to_add]

        if len(watched_to_delete) > 0:
            DB.WATCH_HISTORY_ADVANCED.delete(watched_to_delete)

        if len(ids) > 0:
            api_res = self.api.get_all_pages(self.api.FILTER.ids(ids, False))
            if not api_res:
                return
            for _id in ids:
                exists = False
                for i in api_res['data']:
                    if _id == i['_id']:
                        exists = True
                        break
                if not exists:
                    DB.FILES.delete(_id)
            media_to_add = []
            for i in api_res['data']:
                source = API.get_source(i)
                info_labels = source.get('info_labels', {})
                media_to_add.append([
                    i['_id'],
                    router.get_stream_url(i['_id']),
                    source.get('parent_id'),
                    source.get('root_parent'),
                    info_labels.get('season'),
                    info_labels.get('episode'),
                ])
            DB.WATCH_HISTORY_ADVANCED.add_many(media_to_add)

        DB.WATCH_HISTORY_ADVANCED.update_state_many([(item[2], item[4], True if (item[3] and item[6] is None) else None) for item in files])

        watched = DB.WATCH_HISTORY_ADVANCED.get_all()
        ids = {}
        media_paths = {}
        for item in watched:
            media_id = item[3] or item[0]
            file_item = files_map[item[1]]
            play_count = int(bool(file_item[3] and file_item[6] is None))
            ids[media_id] = ids.get(media_id, 0) + (1 if play_count else 0)
            media_paths[item[0]] = item

        ids_keys = ids.keys()
        api_res = None
        if len(ids_keys) > 0:
            api_res = self.api.get_all_pages(self.api.FILTER.ids(ids.keys(), False))
        if not api_res:
            self.items_to_watch = filtered
            return filtered

        for i in api_res['data']:
            source = API.get_source(i)
            media_id = i["_id"]
            total = source.get("total_children_count", 0)
            children_count = source.get("children_count", 0)
            available = source.get("available_streams", {}).get("count", 0)
            total = total - children_count if children_count != total else total
            if available <= 0:
                continue
            if total and total > 0:
                if ids.get(media_id) < total:
                    filtered.append(media_id)
            else:
                mapped = media_paths.get(media_id)
                if mapped:
                    play_count = ids[media_id]
                    if not play_count:
                        filtered.append(media_id)

        self.items_to_watch = filtered
        cur_path = current_path()
        if not cur_path or ROUTE.CONTINUE_WATCHING == cur_path:
            refresh()
        return filtered

    def sync(self):
        if not DB.FILES.active:
            return
        self.start(WATCH_HISTORY.SYNC_INTERVAL, self.sync_db)
